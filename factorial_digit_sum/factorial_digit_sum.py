# Problem Statement :
# n! means n × (n − 1) × ... × 3 × 2 × 1
# For example, 10! = 10 × 9 × ... × 3 × 2 × 1 = 3628800,
# and the sum of the digits in the number 10! is 3 + 6 + 2 + 8 + 8 + 0 + 0 = 27.
# Find the sum of the digits in the number 100!

num = 2
factorial = 1
sum1 = 0

#factor = int(input("Enter the number to be factorised: "))
factor = 10

for factor in range(2,factor+1):
    factorial = factorial * num
    num = num + 1

print("Factorial is : ",factorial)

factorial = str(factorial)

sum1 = sum(int(i) for i in factorial)

print("Sum of all numbers in a factorial is : ",sum1)
