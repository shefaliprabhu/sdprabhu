# Problem Statement :
# 2 ** 15 = 32768 and the sum of its digits is 3 + 2 + 7 + 6 + 8 = 26.
# What is the sum of the digits of the number 2 ** 1000?

base = 2
power = 1000
product = 0
total = 0

#base = int(input("Enter the base number: "))

#power = int(input("Enter the power: "))

product = base ** power
print("Product is : ", product)

product = str(product)

total = sum(int(i) for i in product)

print("Total = ",total)

