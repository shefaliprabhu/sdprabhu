# Problem Statement :
# Find the difference between the sum of the squares of the first one hundred natural numbers and the square of the sum.

num = 0
sum1 = 0
sum2 = 0
product = 0
difference = 0

for num in range(1,100):
    sum1 = sum1 + (num * num)

print('Sum of squares = ',sum1)

for num in range(1,100):
    sum2 = sum2 + num
    product = sum2 * sum2

print('Squares of sum = ',product)

difference = product - sum1

print('Difference = ',difference)
